package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * Cette classe permet l'instanciation d'objets portant des méthodes liées à la sérialisation d'objets.
 */
public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer {

    ByteArrayOutputStream outTab;

    /**
     * Ce constructeur permet l'instanciation d'objets de classe ImageDeserializerBase64StreamingImpl prennant en parametre un ByteArrayOutputStream et en l'allouant à son attrubut de classe outTab.
     * @param deserializationOutput
     */
    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
        this.outTab = deserializationOutput;
    }

    /**
     * Cette methode est un getter retournant l'attribut outTab de l'objet instancié, de type ByteArrayOutputStream
     * @return ByteArrayOutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return this.outTab;
    }

    /**
     * Cette methode copie les données du ByteArrayOutputStream attribut de l'ImageByteArrayFrame passé en parametre vers la variable outTab de l'objet instancié.
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(ImageByteArrayFrame media) throws IOException {
    getDeserializingStream(media).transferTo(this.outTab);
    }


    /**
     *Cette methode utilise la methose getEncodedImageInput() de l'ImageByteArrayFrame en paramètre et génere un flux Input décodé de type InpuStream
     * @param media
     * @return InputStream
     * @throws IOException
     */

    @Override
    public InputStream getDeserializingStream(ImageByteArrayFrame media) throws IOException {
       return new Base64InputStream(media.getEncodedImageInput());
    }
}
