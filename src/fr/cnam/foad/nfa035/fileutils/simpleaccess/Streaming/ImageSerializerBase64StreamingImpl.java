package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;


/**
 * Cette classe propose 3 methodes permettant de sérialiser des objets de type File et de tranferer le contenu encodé vers un ImageByteArrayFrame
 */
public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer {

    /**
     * Cette méthode fait passer les données du flux de lecture retourné par la méthode getSourceInputStream sur son pparamètre source de type File, vers le flux d'écriture encodé en base 64 retourné par la méthode getSerializingStream sur son paramètre media de type ImageByteArrayFrame
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(File source, ImageByteArrayFrame media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream(media));
    }

    /**
     * Cette méthode retourne un flux d'écriture encodé en base 64 à partir de l'attribut de classe inTab du ImageByteArrayFrame passé en paramètre
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException {
        return new Base64OutputStream(media.inTab);
    }

    /**
     * Cette méthode retourne un flux de lecture InputStreal à partir du File passé en paramètre
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws IOException {
        return new FileInputStream(source) {
        };
    }
}
