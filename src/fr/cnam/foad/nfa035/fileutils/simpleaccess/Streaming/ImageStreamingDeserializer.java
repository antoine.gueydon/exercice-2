package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Cette interface permet l'utilisation de méthodes utiles pour désérialiser des objets de type ImageByteArrayFrame.
 * @param <M>
 */
public interface ImageStreamingDeserializer<M> {


    <T extends OutputStream> T getSourceOutputStream();

    InputStream getDeserializingStream(ImageByteArrayFrame media) throws IOException;

    void deserialize(ImageByteArrayFrame media) throws IOException;
}