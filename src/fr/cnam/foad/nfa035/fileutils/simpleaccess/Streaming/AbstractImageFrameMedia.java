package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Cette classe abstraite prepose 2 methodes à ses filles pour récuperer des objets OuputStream et InputStream
 * @param <T>
 */
public abstract class AbstractImageFrameMedia<T> {

    /**
     * Cette methode retourne un OuputStream et reste à implémenter chez les classes filles
      * @return OuputStream
     * @throws IOException
     */

    public abstract OutputStream getEncodedImageOutput() throws IOException;

    /**
     * Cette methode retourne un InpuStream et reste à implémenter chez les classes filles
     * @return InpuStream
     * @throws IOException
     */
    public abstract InputStream getEncodedImageInput() throws IOException;
}