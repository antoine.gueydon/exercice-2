package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Cette interface permet l'utilisation de méthodes utiles pour sérialiser des objets de type File vers des objets de type ImageByteArrayFrame.
 * @param <File>
 * @param <ImageByteArrayFrame>
 */
public interface ImageStreamingSerializer<File, ImageByteArrayFrame> {



    void serialize(java.io.File source, fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming.ImageByteArrayFrame media) throws IOException;

    OutputStream getSerializingStream(fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming.ImageByteArrayFrame media) throws IOException;

    InputStream getSourceInputStream(java.io.File source) throws IOException;
}
