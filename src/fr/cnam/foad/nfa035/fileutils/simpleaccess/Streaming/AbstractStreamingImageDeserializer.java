package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.IOException;

/**
 * Cette classe abstraite propose et définit à ses filles une methode permettant de désérialiser des Onjets de type ImageByteArrayFrame.
 * @param <ImageByteArrayFrame>
 */
public abstract class AbstractStreamingImageDeserializer<ImageByteArrayFrame> implements ImageStreamingDeserializer<fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming.ImageByteArrayFrame> {

    /**
     * Cette méthode copie les données InputStream depuis le résultat de la methode getDeserializingStream() sur son parametre media de type ImageByteArrayFrame, vers le résultat de les méthode getOutputStream() pour atteindre l'OuputStream cible.
     * @param media
     * @throws IOException
     */


    public final void deserialize(fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming.ImageByteArrayFrame media) throws IOException {


    }
}