package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.File;
import java.io.IOException;

/**
 *      * Cette classe abstraite propose et définit une méthode permettant de sérialiser un objet de type File vers un Objet ImageByteArrayFrame
 * @param <File>
 * @param <ImageByteArrayFrame>
 */

public abstract class AbstractStreamingImageSerializer<File,ImageByteArrayFrame> implements ImageStreamingSerializer<File,ImageByteArrayFrame> {
    /**
     * Cette methode copie les données InputStream depuis le résultat de la méthode getSourceInputStream sur son parametre de type File, vers l'OuputStream résulat de la fonction getSerializingStream() sur son paramétre media
     * @param source
     * @param media
     * @throws IOException
     */

    public final void serialize(File source, ImageByteArrayFrame media) throws IOException {

    }
}