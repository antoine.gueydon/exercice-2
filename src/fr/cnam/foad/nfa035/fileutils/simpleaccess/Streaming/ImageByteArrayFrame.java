package fr.cnam.foad.nfa035.fileutils.simpleaccess.Streaming;

import java.io.*;

/**
 * Cette classe permet l'instanciation d'objet contenant un attribut ByteArrayOuputStream. Elle definit 2 méthodes pour retourner un flux en lecture ou en écriture de cet attribut
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia{
    ByteArrayOutputStream inTab;

    /**
     * Contructeur permettant l'instanciation d'objets en affectant le parametre ByteArrayOutputStream dans son attribut de classe
     * @param inTab
     */

    public ImageByteArrayFrame(ByteArrayOutputStream inTab) {
        this.inTab = inTab;
    }

    /**
     * Cette methode est un getter retournant l'attribut de classe de l'objet instancié
     * @return ByteArrayOutputStream
     */
    public OutputStream getEncodedImageOutput() {
    return this.inTab;
    }

    /**
     * Cette méthode écrit le flux porté par l'attribut de classe de l'objet ImageFrameMedia instancié, donc un objet de type ByteArrayOuputStrea, dans un tableau de type byte[]. Elle retourne ensuite un flux de lecture issu de ce byte[].
     * @return ByteArrayInputStream
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        ByteArrayOutputStream outStream = this.inTab;
        byte[] encodedByte = outStream.toByteArray();
        return new ByteArrayInputStream(encodedByte);
    }
}
